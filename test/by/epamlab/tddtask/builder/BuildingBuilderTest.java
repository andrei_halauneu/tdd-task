package by.epamlab.tddtask.builder;

import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.controller.ElevatorController;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

/**
 * Created by Andrei Halauneu on 05.08.2016.
 */
public class BuildingBuilderTest {
    private static final int PARAMETERS_COUNT = 3;

    private int passengersNumber = BuildingBuilder.getValue(BuildingBuilder.KEY_PASSENGERS_NUMBER);
    private int floorsNumber = BuildingBuilder.getValue(BuildingBuilder.KEY_STORIES_NUMBER);
    private BuildingBuilder builder = new BuildingBuilder();

    @Test
    public void ReturnsSameNumberOfParametersAsInThePropertyFile() throws Exception {
        ArrayList<Integer> parameters = new ArrayList<>();
        parameters.add(BuildingBuilder.getValue(BuildingBuilder.KEY_STORIES_NUMBER));
        parameters.add(BuildingBuilder.getValue(BuildingBuilder.KEY_ELEVATOR_CAPACITY));
        parameters.add(BuildingBuilder.getValue(BuildingBuilder.KEY_PASSENGERS_NUMBER));
        assertThat("Get same number of parameters as in the property file", parameters.size(), is(PARAMETERS_COUNT));
    }

    @Test
    public void createsPassengersAndReturnsSameNumberAsContains() throws Exception {
        assertThat("The number of created passengers same as contains",
                builder.createPassengers(passengersNumber).size(), is(passengersNumber));
    }

    @Test
    public void createsPassengersWithSourceFloorNotEqualTargetFloor() throws Exception {
        for (Passenger passenger : builder.createPassengers(passengersNumber)) {
            assertNotEquals("Passenger's source floor is not equal target floor",
                    passenger.getSource(), passenger.getTarget());
        }
    }

    @Test
    public void createsPassengersWithUniqueId() throws Exception {
        List<Passenger> passengers = builder.createPassengers(passengersNumber);
        Set<Passenger> setWithoutDuplicates = new HashSet<>(passengers);
        assertThat("Passengers was created with unique id", passengers.size(),
                is(setWithoutDuplicates.size()));
    }

    @Test
    public void createsFloorsWithDispatcherContainers() throws Exception {
        boolean created = false;
        for (Floor floor : builder.createFloors(floorsNumber)) {
            if (floor.getDispatchStoryContainer() == null) {
                created = false;
                break;
            } else {
                created = true;
            }
        }
        assertThat("Floors were created with dispatch container", created, is(true));
    }

    @Test
    public void PlacesPassengersAndChecksThatTheyAreNotOnTheSameFloor() throws Exception {
        List<Passenger> passengers = builder.createPassengers(passengersNumber);
        List<Floor> floors = builder.createFloors(floorsNumber);
        builder.placePassengersToFloors(passengers, floors);
        for (Floor floor : floors) {
            assertNotEquals("Passengers are not ath the same floor",
                    floor.getDispatchStoryContainer().getPassengersCount(),
                    is(passengersNumber));
        }
    }

    @Test
    public void createsTransportationTaskForEachPassenger() throws Exception {
        List<Passenger> passengers = builder.createPassengers(passengersNumber);
        for (Passenger passenger : passengers) {
            assertNotNull("Passenger has transportation task",
                    passenger.getTransportationTask());
        }
    }

    @Test
    public void createsElevatorControllerWithElevator() throws Exception {
        ElevatorController elevatorController = builder.createElevatorController();
        assertNotNull("Elevator controller has elevator", elevatorController.getElevator());

    }
}
