package by.epamlab.tddtask.utils;

import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Elevator;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.controller.ElevatorController;
import by.epamlab.tddtask.enums.TransportationState;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Andrei Halauneu on 11.08.2016.
 */
public class CompleteTransportationValidatorTest {
    private CompleteTransportationValidator validator = new CompleteTransportationValidator();

    @Before
    public void setUp() throws Exception {
        validator.setFloors(new ArrayList<>());
        validator.setPassengers(new ArrayList<>());
        validator.setElevator(new Elevator());
    }

    @Test
    public void validatesTrueIfAllDispatcherContainersIsEmpty() throws Exception {
        Floor floor = mock(Floor.class);
        List<Floor> floors = createFloors(3);
        validator.setFloors(floors);
        when(floor.getDispatchContainerSize()).thenReturn(0);
        assertThat("All dispatcher containers must be empty",
                validator.isDispatcherContainersEmpty(),
                is(true));
    }

    @Test
    public void validatesTrueIfElevatorContainerIsEmpty() throws Exception {
        Elevator elevator = mock(Elevator.class);
        validator.setElevator(elevator);
        when(elevator.getNumberOfPassengersInside()).thenReturn(0);
        ElevatorController controller = new ElevatorController(elevator);
        assertThat("Elevator container must be empty",
                validator.isElevatorContainerEmpty(),
                is(true));
    }

    @Test
    public void setsPassengersTransportationStateToCompleted() throws Exception {
        Passenger passenger = mock(Passenger.class);
        when(passenger.getTransportationState()).thenReturn(TransportationState.COMPLETED);
        validator.setPassengers(Arrays.asList(passenger));
        assertThat("Passengers transportation state is COMPLETED",
                validator.isPassengersStateCompleted(),
                is(true));
    }

    @Test
    public void validatesTrueIfPassengersTargetFloorsIsEqualToFloorNumbers() throws Exception {
        Floor floor = new Floor(1);
        Passenger passenger = new Passenger(1, 0, 1);
        floor.addPassengersToArrivalContainer(Arrays.asList(passenger));
        validator.setFloors(Arrays.asList(floor));
        assertThat("Passengers target floors is equal to floor Numbers",
                validator.isPassengersOnRightFloors(),
                is(true));

    }

    @Test
    public void validatesTrueIfPassengersNumberInArrivalContainersIsEqualToPassengersNumber() throws Exception {
        Floor floor = mock(Floor.class);
        List<Floor> floors = new ArrayList<>();
        for (int i = 0; i < Building.FLOORS_NUMBER; i++) {
            floors.add(floor);
        }
        when(floor.getArrivalContainerSize()).thenReturn(2);
        validator.setFloors(floors);
        assertThat("Passengers number in arrival containers is equal to passengers number",
                validator.isPassengersNumberEqual(),
                is(true));
    }

    private List<Floor> createFloors(int floorsCount) {
        List<Floor> floors = new ArrayList<>();
        for (int i = 0; i < floorsCount; i++) {
            floors.add(new Floor(i));
        }
        return floors;
    }
}
