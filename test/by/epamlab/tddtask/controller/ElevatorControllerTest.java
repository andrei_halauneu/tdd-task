package by.epamlab.tddtask.controller;

import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Elevator;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.builder.BuildingBuilder;
import by.epamlab.tddtask.enums.ElevatorState;
import by.epamlab.tddtask.enums.TransportationState;
import by.epamlab.tddtask.task.TransportationTask;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by Andrei Halauneu on 05.08.2016.
 */
public class ElevatorControllerTest {
    private ElevatorController elevatorController;

    private BuildingBuilder builder = new BuildingBuilder();
    private Building building;
    private int passengersNumber = BuildingBuilder.getValue(BuildingBuilder.KEY_PASSENGERS_NUMBER);
    private int floorsNumber = BuildingBuilder.getValue(BuildingBuilder.KEY_STORIES_NUMBER);

    @Before
    public void setUp() throws Exception {
        setUpBuilding();
        elevatorController = building.getElevatorController();
    }

    @Test
    public void movesElevatorWithChangingCurrentFloor() throws Exception {
        List<Floor> floors = createFloors(10);
        for (Floor floor : floors) {
            elevatorController.moveElevatorUp(floor);
        }
        assertThat("Controller changed current floor",
                elevatorController.getElevator().getCurrentFloorNumber(),
                is(9));
    }

    @Test
    public void movesElevatorWithMovingElevatorState() throws Exception {
        List<Floor> floors = createFloors(2);
        for (Floor floor : floors) {
            elevatorController.moveElevatorUp(floor);
        }
        assertThat("Elevator state is equal MOVING_ELEVATOR",
                elevatorController.getElevator().getElevatorState(),
                is(ElevatorState.MOVING_ELEVATOR));

    }

    @Test
    public void stopsElevatorWithDeboardingOfPassengersState() throws Exception {
        elevatorController.stopElevator();
        assertThat("Elevator stopped and its state is DEBOARDING_OF_PASSENGER",
                elevatorController.getElevator().getElevatorState(),
                is(ElevatorState.DEBOARDING_OF_PASSENGER));

    }

    @Test
    public void stopsElevatorAndNotifiesPassengers() throws Exception {
        addPassengersToElevator(elevatorController, building.getPassengers());
        elevatorController.stopElevator();
        elevatorController.notifyPassengersInside(new CountDownLatch(0));
        Collection<Passenger> passengersInside = elevatorController.getElevator()
                .getPassengersInside();
        for (Passenger passenger : passengersInside) {
            assertThat("Notifies passengers inside", passenger.isNotified(), is(true));
        }
        for (Passenger passenger : building.getPassengers()) {
            assertThat("Notifies passengers outside", passenger.isNotified(), is(false));
        }
    }

    @Test
    public void passengersAskToGetOutWithTargetFloorSameAsCurrentFloor() throws Exception {
        boolean isSameFloors = false;
        ExecutorService executorService = Executors.newCachedThreadPool();
        addPassengersToElevator(elevatorController, building.getPassengers());

        for (Floor floor : building.getFloors()) {
            CountDownLatch countDownLatch = new CountDownLatch(building.getElevator()
                    .getNumberOfPassengersInside());
            elevatorController.moveElevatorUp(floor);
            elevatorController.stopElevator();
            for (TransportationTask task : createsPassengersAskingToGetOut(elevatorController, countDownLatch)) {
                executorService.submit(task);
            }
            countDownLatch.await();
            elevatorController.notifyPassengersInside(countDownLatch);
            List<Passenger> passengersToGetOut = elevatorController.getPassengersToGetOut();
            if (!passengersToGetOut.isEmpty()) {
                for (Passenger passenger : passengersToGetOut) {
                    if (passenger.getTarget() == building.getElevator().getCurrentFloorNumber()) {
                        isSameFloors = true;
                    } else {
                        isSameFloors = false;
                        assertThat("Passenger has target floor same as current floor", isSameFloors, is(false));
                        break;
                    }
                }
                resetCandidateToGetOut(elevatorController);
            }
        }
        assertThat("Passenger has target floor same as a current floor", isSameFloors, is(true));
        executorService.shutdown();
    }

    @Test(timeout = 5000)
    public void getsOutPassengerWithPassengerTransportationStateIsCompleted() throws Exception {
        addPassengersToElevator(elevatorController, building.getPassengers());
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (Floor floor : building.getFloors()) {
            CountDownLatch countDownLatch = new CountDownLatch(building.getElevator()
                    .getNumberOfPassengersInside());
            elevatorController.moveElevatorUp(floor);
            elevatorController.stopElevator();
            elevatorController.notifyPassengersInside(countDownLatch);
            for (TransportationTask task : createsPassengersReadyToGetOut(elevatorController, countDownLatch)) {
                executorService.submit(task);
            }
            countDownLatch.await();
            executorService.awaitTermination(10, TimeUnit.MILLISECONDS);
            List<Passenger> passengers = elevatorController.getPassengersToGetOut();
            if (!passengers.isEmpty()) {
                for (Passenger passenger : passengers) {
                    assertThat("Passenger transportation state is COMPLETED",
                            passenger.getTransportationState(),
                            is(TransportationState.COMPLETED));
                }
            }
        }
        executorService.shutdown();
    }

    @Test
    public void getsOutOnePassengerAndIncrementsArrivalStorageSize() throws Exception {
        addPassengersToElevator(elevatorController, building.getPassengers());
        for (Floor floor : building.getFloors()) {
            elevatorController.moveElevatorUp(floor);
            elevatorController.stopElevator();
            elevatorController.notifyPassengersInside(new CountDownLatch(0));
            for (Passenger passenger : elevatorController.getElevator().getPassengersInside()) {
                if (passenger.getTarget() == floor.getFloorNumber()) {
                    int arrivalContainerSize = floor.getArrivalContainerSize();
                    if (passenger.getElevatorController().askToGetOut(passenger)) {
                        passenger.getElevatorController().getOut(passenger, floor);
                        assertThat("Arrival storage size was incremented after get out",
                                floor.getArrivalContainerSize(),
                                is(arrivalContainerSize + 1));
                    }
                }
            }
        }
    }

    @Test
    public void getsOutPassengerDecrementsElevatorContainerSize() throws Exception {
        addPassengersToElevator(elevatorController, building.getPassengers());
        for (Floor floor : building.getFloors()) {
            elevatorController.moveElevatorUp(floor);
            elevatorController.stopElevator();
            elevatorController.notifyPassengersInside(new CountDownLatch(0));
            for (Passenger passenger : elevatorController.getElevator().getPassengersInside()) {
                passenger.setElevatorController(elevatorController);
                if (passenger.getTarget() == floor.getFloorNumber()) {
                    int elevatorContainerSize = elevatorController.getElevator().getNumberOfPassengersInside();
                    if (passenger.getElevatorController().askToGetOut(passenger)) {
                        passenger.getElevatorController().getOut(passenger, floor);
                        assertThat("Elevator container size was decremented after get out",
                                elevatorController.getElevator().getNumberOfPassengersInside(),
                                is(elevatorContainerSize - 1));
                    }
                }
            }
        }
    }

    @Test
    public void NotifiesPassengersOnTheFloorWithBoardingOfPassengerElevatorState() throws Exception {
        Building building = mock(Building.class);
        when(building.getFloors()).thenReturn(createFloors(2));
        for (Floor floor : building.getFloors()) {
            elevatorController.notifyPassengersOutside(floor, new CountDownLatch(0));
            assertThat("Notifies passengers on the floor with BOARDING_OF_PASSENGER elevator state",
                    elevatorController.getElevator().getElevatorState(),
                    is(ElevatorState.BOARDING_OF_PASSENGER));
        }
    }

    @Test
    public void notifiesPassengersOnTheCurrentFloorOnly() throws Exception {
        Building building = mock(Building.class);
        when(building.getFloors()).thenReturn(createFloors(2));

        for (Floor floor : building.getFloors()) {
            floor.addPassengersToDispatcherContainer(createPassengers(1));
            elevatorController.notifyPassengersOutside(floor, new CountDownLatch(0));
            for (Passenger passenger : floor.getDispatchStoryContainer().getPassengers()) {
                assertThat("Notify passengers on the current floor only",
                        passenger.isNotified(), is(true));
            }
        }
    }

    @Test
    public void changesElevatorStateToMovingElevatorIfElevatorContainerIsFull() throws Exception {
        Elevator elevator = mock(Elevator.class);
        ElevatorController controller = new ElevatorController(elevator);

        when(elevator.isElevatorFull()).thenReturn(true);

        Floor floor = new Floor(2);
        floor.addPassengersToDispatcherContainer(createPassengers(1));
        for (Passenger passenger : floor.getDispatchStoryContainer().getPassengers()) {
            passenger.setElevatorController(controller);
            passenger.getElevatorController().askToGetIn();
        }

        verify(elevator).setElevatorState(ElevatorState.MOVING_ELEVATOR);
    }

    @Test
    public void changesElevatorStateToMovingElevatorIfArrivalContainerIsEmpty() throws Exception {
        Elevator elevator = mock(Elevator.class);
        ElevatorController elevatorController = new ElevatorController(elevator);
        Floor floor = mock(Floor.class);

        when(elevator.isElevatorFull()).thenReturn(false);
        when(elevator.getCurrentFloor()).thenReturn(floor);
        when(floor.isFloorEmpty()).thenReturn(true);
        when(floor.getPassengersFromArrivalContainer()).thenReturn(createPassengers(1));

        for (Passenger passenger : floor.getPassengersFromArrivalContainer()) {
            passenger.setElevatorController(elevatorController);
            passenger.getElevatorController().askToGetIn();
        }
        verify(elevator).setElevatorState(ElevatorState.MOVING_ELEVATOR);
    }

    @Test
    public void getsInPassengersWithSourceFloorEqualCurrentFloor() throws Exception {
        builder.placePassengersToFloors(building.getPassengers(), building.getFloors());

        for (Floor floor : building.getFloors()) {
            elevatorController.moveElevatorUp(floor);
            elevatorController.stopElevator();
            elevatorController.notifyPassengersOutside(floor, new CountDownLatch(0));
            for (Passenger passenger : floor.getPassengersFromDispatchContainer()) {
                passenger.setElevatorController(elevatorController);
                if (passenger.getElevatorController().askToGetIn()) {
                    passenger.getElevatorController().getIn(passenger, floor);
                    assertThat("Get in passenger with source floor equal current floor",
                            passenger.getSource(),
                            is(floor.getFloorNumber()));
                }
            }
        }
    }

    @Test
    public void getsInOnePassengerAndIncrementsElevatorContainerSize() throws Exception {
        List<Passenger> passengers = createPassengers(1);
        ElevatorController spyElevatorController = spy(new ElevatorController(building.getElevator()));

        Floor floor = new Floor(0);
        floor.addPassengersToArrivalContainer(passengers);
        int elevatorContainerSize = spyElevatorController.getElevator().getNumberOfPassengersInside();

        doAnswer(Answers.CALLS_REAL_METHODS.get()).when(spyElevatorController).getIn(any(), any());
        when(spyElevatorController.askToGetIn()).thenReturn(true);

        Passenger passenger = passengers.get(0);
        passenger.setElevatorController(spyElevatorController);
        if (passenger.getElevatorController().askToGetIn()) {
            passenger.getElevatorController().getIn(passenger, floor);
            assertThat("Get in one passenger and increment elevator container size",
                    spyElevatorController.getElevator().getNumberOfPassengersInside(),
                    is(elevatorContainerSize + 1));
        }
    }

    @Test
    public void getsInOnePassengerAndDecrementsDispatchStorageSize() throws Exception {
        List<Passenger> passengers = createPassengers(1);
        ElevatorController spyElevatorController = spy(new ElevatorController(building.getElevator()));

        Floor floor = new Floor(0);
        floor.addPassengersToDispatcherContainer(passengers);
        int dispatchContainerSize = floor.getDispatchContainerSize();

        doAnswer(Answers.CALLS_REAL_METHODS.get()).when(spyElevatorController).getIn(any(), any());
        when(spyElevatorController.askToGetIn()).thenReturn(true);

        Passenger passenger = passengers.get(0);
        passenger.setElevatorController(spyElevatorController);
        if (passenger.getElevatorController().askToGetIn()) {
            passenger.getElevatorController().getIn(passenger, floor);
            assertThat("Get in one passenger and decrement dispatch container size",
                    floor.getDispatchContainerSize(),
                    is(dispatchContainerSize - 1));
        }

    }

    private List<Passenger> createPassengers(int passengersCount) {
        Random random = new Random(0);
        List<Passenger> passengers = new ArrayList<>();
        for (int i = 0; i < passengersCount; i++) {
            passengers.add(new Passenger(i,
                    random.nextInt(Building.FLOORS_NUMBER),
                    random.nextInt(Building.FLOORS_NUMBER)));
        }
        return passengers;
    }

    private List<TransportationTask> createsPassengersReadyToGetOut(ElevatorController elevatorController, CountDownLatch countDownLatch) {
        List<TransportationTask> transportationTasks = new ArrayList<>();
        for (Passenger passenger : elevatorController.getElevator().getPassengersInside()) {
            transportationTasks.add(new TransportationTask(passenger) {
                @Override
                public Passenger call() throws Exception {
                    countDownLatch.countDown();
                    passenger.setElevatorController(elevatorController);
                    if (passenger.getElevatorController().getElevator().getElevatorState()
                            == ElevatorState.DEBOARDING_OF_PASSENGER) {
                        if (passenger.getElevatorController().askToGetOut(passenger)) {
                            passenger.getElevatorController().getOut(passenger,
                                    elevatorController.getElevator().getCurrentFloor());
                            passenger.setTransportationState(TransportationState.COMPLETED);
                        }
                    }
                    return passenger;
                }
            });
        }
        return transportationTasks;
    }

    private List<TransportationTask> createsPassengersAskingToGetOut(ElevatorController elevatorController,
                                                                     CountDownLatch countDownLatch) {
        List<TransportationTask> transportationTasks = new ArrayList<>();
        for (Passenger passenger : elevatorController.getElevator().getPassengersInside()) {
            transportationTasks.add(new TransportationTask(passenger) {
                @Override
                public Passenger call() throws Exception {
                    countDownLatch.countDown();
                    passenger.setElevatorController(elevatorController);
                    if (passenger.getElevatorController().getElevator().getElevatorState()
                            .equals(ElevatorState.DEBOARDING_OF_PASSENGER)) {
                        passenger.getElevatorController().askToGetOut(passenger);
                    }
                    return passenger;
                }
            });
        }
        return transportationTasks;
    }

    private void addPassengersToElevator(ElevatorController elevatorController, List<Passenger> passengers) {
        for (int i = 0; i < 3; i++) {
            elevatorController.getElevator().getElevatorContainer()
                    .addPassenger(passengers.get(i));
            passengers.get(i).setElevatorController(elevatorController);
            passengers.remove(i);
        }
    }

    private List<Floor> createFloors(int floorsCount) {
        List<Floor> floors = new ArrayList<>();
        for (int i = 0; i < floorsCount; i++) {
            floors.add(new Floor(i));
        }
        return floors;
    }

    private void resetCandidateToGetOut(final ElevatorController elevatorController) {
        elevatorController.getPassengersToGetOut().clear();
    }

    private void setUpBuilding() {
        building = new Building(builder.createPassengers(passengersNumber),
                builder.createElevatorController(),
                builder.createFloors(floorsNumber));
    }
}
