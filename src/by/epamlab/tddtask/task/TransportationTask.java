package by.epamlab.tddtask.task;

import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.controller.ElevatorController;
import by.epamlab.tddtask.enums.TransportationState;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class TransportationTask implements Callable<Passenger> {
    private Passenger passenger;
    private CountDownLatch countDownLatch;
    private CountDownLatch passengerCountDownLatch;

    /**
     * Creates transportation task with passenger
     *
     * @param passenger passenger
     */
    public TransportationTask(final Passenger passenger) {
        this.passenger = passenger;
    }

    /**
     * Creates transportation task with passenger and count down latch
     *
     * @param passenger      passenger
     * @param countDownLatch count down latch
     */
    public TransportationTask(final Passenger passenger, final CountDownLatch countDownLatch) {
        this(passenger);
        this.countDownLatch = countDownLatch;
    }

    @Override
    public Passenger call() throws Exception {
        countDownLatch.countDown();
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                doWait();
                switch (passenger.getElevatorController().getElevator().getElevatorState()) {
                    case DEBOARDING_OF_PASSENGER:
                        tryToGetOut();
                        break;
                    case BOARDING_OF_PASSENGER:
                        tryToGetIn();
                        break;
                    default:
                        passenger.getTransportationTask().getCountDownLatch().countDown();
                }
                if (passenger.getTransportationState().equals(TransportationState.COMPLETED)) {
                    Thread.currentThread().interrupt();
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return passenger;
            }
        }
        return null;
    }

    private void tryToGetOut() {
        ElevatorController elevatorController = passenger.getElevatorController();
        passengerCountDownLatch = passenger.getTransportationTask().getCountDownLatch();
        if (elevatorController.askToGetOut(passenger)) {
            elevatorController.getOut(passenger, elevatorController.getElevator().getCurrentFloor());
            passenger.setTransportationState(TransportationState.COMPLETED);
        }
        passenger.setNotified(false);
        passengerCountDownLatch.countDown();
    }

    private void tryToGetIn() throws InterruptedException {
        ElevatorController elevatorController = passenger.getElevatorController();
        passengerCountDownLatch = passenger.getTransportationTask().getCountDownLatch();
        if (elevatorController.askToGetIn()) {
            elevatorController.getIn(passenger, elevatorController.getElevator().getCurrentFloor());
        }
        passenger.setNotified(false);
        passengerCountDownLatch.countDown();
    }


    private void doWait() throws InterruptedException {
        synchronized (passenger) {
            while (!passenger.isNotified()) {
                passenger.wait();
            }
        }
    }

    /**
     * Sets count down latch
     *
     * @param countDownLatch count down latch
     */
    public void setCountDownLatch(final CountDownLatch countDownLatch) {
        this.passengerCountDownLatch = countDownLatch;
    }

    /**
     * Notifies transportation task
     */
    public void doNotify() {
        synchronized (passenger) {
            passenger.setNotified(true);
            passengerCountDownLatch.countDown();
            passenger.notifyAll();
        }
    }

    private CountDownLatch getCountDownLatch() {
        return this.passengerCountDownLatch;
    }
}
