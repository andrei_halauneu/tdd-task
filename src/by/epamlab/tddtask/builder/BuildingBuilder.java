package by.epamlab.tddtask.builder;

import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Elevator;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.controller.ElevatorController;

import java.util.*;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class BuildingBuilder {
    /**
     * Binds .properties file
     */
    public static final String FILE_NAME = "resources/config";
    /**
     * Stories number key of property file
     */
    public static final String KEY_STORIES_NUMBER = "storiesNumber";
    /**
     * Elevator capacity key of property file
     */
    public static final String KEY_ELEVATOR_CAPACITY = "elevatorCapacity";
    /**
     * Passengers number key of property file
     */
    public static final String KEY_PASSENGERS_NUMBER = "passengersNumber";

    private static final ResourceBundle RESOURCE = ResourceBundle.getBundle(FILE_NAME);

    private Random generator = new Random();

    /**
     * Gets value from property file by key
     *
     * @param key key of property
     * @return value of property
     */
    public static int getValue(final String key) {
        return Integer.parseInt(RESOURCE.getString(key));
    }

    /**
     * Creates specified number of passengers with random source and target floors
     *
     * @param passengersNumber number of passenger
     * @return list of passengers
     */
    public List<Passenger> createPassengers(final int passengersNumber) {
        List<Passenger> passengers = new ArrayList<>();
        for (int i = 0; i < passengersNumber; i++) {
            int sourceFloor;
            int targetFloor;
            do {
                sourceFloor = getRandomFloorNumber(Building.FLOORS_NUMBER);
                targetFloor = getRandomFloorNumber(Building.FLOORS_NUMBER);
            } while (sourceFloor == targetFloor);
            passengers.add(new Passenger(i, sourceFloor, targetFloor));
        }
        return passengers;
    }

    private int getRandomFloorNumber(final int floorsNumber) {
        return generator.nextInt(floorsNumber);
    }

    /**
     * Creates specified number of floors
     *
     * @param floorsNumber number of floors
     * @return list of floors
     */
    public List<Floor> createFloors(final int floorsNumber) {
        List<Floor> floors = new ArrayList<>();
        for (int i = 0; i < floorsNumber; i++) {
            floors.add(new Floor(i));
        }

        return floors;
    }

    /**
     * Place passengers to the floors
     *
     * @param passengers passengers to place
     * @param floors     floors to place on
     */
    public void placePassengersToFloors(final List<Passenger> passengers, final List<Floor> floors) {
        for (Floor floor : floors) {
            for (Passenger passenger : passengers) {
                if (floor.getFloorNumber() == passenger.getSource()) {
                    floor.getDispatchStoryContainer().addPassenger(passenger);
                }
            }
        }
    }

    /**
     * Creates elevator controller
     *
     * @return elevator controller
     */
    public ElevatorController createElevatorController() {
        return new ElevatorController(new Elevator());
    }
}
