package by.epamlab.tddtask;

import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.builder.BuildingBuilder;
import by.epamlab.tddtask.task.TransportationTask;
import by.epamlab.tddtask.utils.CompleteTransportationValidator;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public final class Runner {
    private Runner() { }
    /**
     * Main method
     * @param args param
     * @throws InterruptedException exception
     */
    public static void main(final String[] args) throws InterruptedException {
        BuildingBuilder builder = new BuildingBuilder();
        Building building = new Building(
                builder.createPassengers(builder.getValue(BuildingBuilder.KEY_PASSENGERS_NUMBER)),
                builder.createElevatorController(),
                builder.createFloors(builder.getValue(BuildingBuilder.KEY_STORIES_NUMBER)));

        List<Passenger> passengers = building.getPassengers();
        List<Floor> floors = building.getFloors();
        builder.placePassengersToFloors(passengers, floors);

        for (Passenger passenger : passengers) {
            passenger.setElevatorController(building.getElevatorController());
        }

        CompleteTransportationValidator validator = CompleteTransportationValidator.getInstance();
        validator.setFloors(floors);
        validator.setPassengers(passengers);
        validator.setElevator(building.getElevator());

        ExecutorService executor = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(Building.PASSENGERS_NUMBER);
        for (Passenger passenger : building.getPassengers()) {
            executor.submit(new TransportationTask(passenger, latch));
        }

        latch.await();

        executor.execute(building.getElevatorController());
        executor.shutdown();
    }
}
