package by.epamlab.tddtask.utils;

import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Elevator;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.enums.TransportationState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei Halauneu on 11.08.2016.
 */
public class CompleteTransportationValidator {

    private static CompleteTransportationValidator instance;

    private List<Floor> floors = new ArrayList<>();
    private List<Passenger> passengers = new ArrayList<>();
    private Elevator elevator = new Elevator();

    /**
     * Returns static instance of validator
     *
     * @return instance of validator
     */
    public static CompleteTransportationValidator getInstance() {
        if (instance == null) {
            instance = new CompleteTransportationValidator();
        }
        return instance;
    }

    /**
     * Sets floors to validate
     *
     * @param floors floors
     */
    public void setFloors(final List<Floor> floors) {
        this.floors = floors;
    }

    /**
     * Sets passengers to validate
     *
     * @param passengers passengers
     */
    public void setPassengers(final List<Passenger> passengers) {
        this.passengers = passengers;
    }

    /**
     * Sets elevator to validates
     *
     * @param elevator elevator
     */
    public void setElevator(final Elevator elevator) {
        this.elevator = elevator;
    }

    /**
     * Checks that each dispatcher container is empty
     *
     * @return true if empty, otherwise false
     */
    public boolean isDispatcherContainersEmpty() {
        for (Floor floor : floors) {
            if (!floor.isFloorEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks that elevator container is empty
     *
     * @return true if empty, otherwise false
     */
    public boolean isElevatorContainerEmpty() {
        return elevator.getNumberOfPassengersInside() == 0;
    }

    /**
     * Checks that passengers state is completed
     *
     * @return true if completed, otherwise false
     */
    public boolean isPassengersStateCompleted() {
        for (Passenger passenger : passengers) {
            if (!passenger.getTransportationState().equals(TransportationState.COMPLETED)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks that passengers on their targets floor
     *
     * @return true if floor number and target is equal, otherwise false
     */
    public boolean isPassengersOnRightFloors() {
        for (Floor floor : floors) {
            for (Passenger passenger : floor.getPassengersFromArrivalContainer()) {
                if (floor.getFloorNumber() != passenger.getTarget()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks that summary count of arrival containers passengers and and passengers number are equal
     *
     * @return true if equal, otherwise false
     */
    public boolean isPassengersNumberEqual() {
        int passengersNumber = 0;
        for (Floor floor : floors) {
            passengersNumber += floor.getArrivalContainerSize();
        }
        return Building.PASSENGERS_NUMBER == passengersNumber;
    }
}
