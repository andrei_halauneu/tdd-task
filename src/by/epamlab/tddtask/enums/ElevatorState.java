package by.epamlab.tddtask.enums;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public enum ElevatorState {
    /**
     * Starts transportation
     */
    STARTING_TRANSPORTATION,
    /**
     * Completed transportation
     */
    COMPLETION_TRANSPORTATION,
    /**
     * Deboards passenger on floor
     */
    DEBOARDING_OF_PASSENGER,
    /**
     * Boards passenger on floor
     */
    BOARDING_OF_PASSENGER,
    /**
     * Moves elevator
     */
    MOVING_ELEVATOR
}
