package by.epamlab.tddtask.enums;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public enum TransportationState {
    /**
     * Sets to passengers after init
     */
    NOT_STARTED,
    /**
     * Sets to passengers when transportation task is created
     */
    IN_PROGRESS,
    /**
     * Sets to passengers when transportation task is destroyed
     */
    COMPLETED
}
