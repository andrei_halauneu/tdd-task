package by.epamlab.tddtask.beans;

import by.epamlab.tddtask.task.TransportationTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class Container {
    private Map<Integer, Passenger> passengers;

    /**
     * Container constructor
     */
    public Container() {
        passengers = new ConcurrentHashMap<>();
    }

    /**
     * Adds passenger
     *
     * @param passenger add
     */
    public void addPassenger(final Passenger passenger) {
        passengers.put(passenger.getId(), passenger);
    }

    /**
     * Getting number of passengers
     *
     * @return count
     */
    public int getPassengersCount() {
        return passengers.size();
    }

    /**
     * Adds tasks for passengers
     *
     * @return transportation tasks
     */
    public List<TransportationTask> getPassengersTasks() {
        List<TransportationTask> transportationTasks = new ArrayList<>();
        for (Passenger passenger : passengers.values()) {
            transportationTasks.add(passenger.getTransportationTask());
        }
        return transportationTasks;
    }

    /**
     * Return number
     *
     * @return number
     */
    public Collection<Passenger> getPassengers() {
        return passengers.values();
    }

    /**
     * Removes passenger
     *
     * @param passenger remove
     */
    public void removePassenger(final Passenger passenger) {
        passengers.remove(passenger.getId());
    }
}
