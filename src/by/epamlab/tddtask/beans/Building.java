package by.epamlab.tddtask.beans;

import by.epamlab.tddtask.builder.BuildingBuilder;
import by.epamlab.tddtask.controller.ElevatorController;

import java.util.*;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class Building {
    /**
     * Number of floors
     */
    public static final int FLOORS_NUMBER = BuildingBuilder.getValue(BuildingBuilder.KEY_STORIES_NUMBER);
    /**
     * Capacity of elevator
     */
    public static final int ELEVATOR_CAPACITY = BuildingBuilder.getValue(BuildingBuilder.KEY_ELEVATOR_CAPACITY);
    /**
     * Number of passengers
     */
    public static final int PASSENGERS_NUMBER = BuildingBuilder.getValue(BuildingBuilder.KEY_PASSENGERS_NUMBER);
    private static List<Floor> floors;
    private static int currentFloor = 0;

    private List<Passenger> passengers;
    private Elevator elevator;
    private ElevatorController controller;

    /**
     * Building constructor from fields
     *
     * @param passengers         passengers
     * @param elevatorController type
     * @param floors             floors
     */
    public Building(
            final List<Passenger> passengers, final ElevatorController elevatorController, final List<Floor> floors) {
        this.passengers = passengers;
        this.elevator = elevatorController.getElevator();
        this.controller = elevatorController;
        Building.floors = floors;
    }

    /**
     * Getter
     *
     * @return floors
     */
    public List<Floor> getFloors() {
        return floors;
    }

    /**
     * Getter
     *
     * @return controller
     */
    public ElevatorController getElevatorController() {
        return controller;
    }

    /**
     * Getter
     *
     * @return passengers
     */
    public List<Passenger> getPassengers() {
        return passengers;
    }

    /**
     * Getter
     *
     * @return elevator
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * Floor leveling up
     *
     * @return floor
     */
    public static Floor nextFloor() {
        try {
            return floors.get(++currentFloor);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Elevator tries to exceed higher level!");
        }
    }

    /**
     * Floor leveling down
     *
     * @return floor
     */
    public static Floor previousFloor() {
        try {
            return floors.get(--currentFloor);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("Elevator tries to exceed lower level!");
        }
    }

    /**
     * Floor reset to lowest
     */
    public static void resetCurrentFloor() {
        currentFloor = 0;
    }
}
