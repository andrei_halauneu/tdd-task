package by.epamlab.tddtask.beans;

import by.epamlab.tddtask.enums.ElevatorState;
import by.epamlab.tddtask.task.TransportationTask;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class Elevator {
    private Floor currentFloor;
    private Container elevatorContainer;
    private ElevatorState elevatorState;

    /**
     * Constructs and sets elevator floor to first
     */
    public Elevator() {
        currentFloor = new Floor(0);
        elevatorContainer = new Container();
    }

    /**
     * Returns elevator container
     *
     * @return container
     */
    public Container getElevatorContainer() {
        return elevatorContainer;
    }

    /**
     * Returns current floor
     *
     * @return number
     */
    public int getCurrentFloorNumber() {
        return currentFloor.getFloorNumber();
    }

    /**
     * @return state
     */
    public ElevatorState getElevatorState() {
        return elevatorState;
    }

    /**
     * Sets new elevator state
     *
     * @param elevatorState state
     */
    public void setElevatorState(final ElevatorState elevatorState) {
        this.elevatorState = elevatorState;
    }

    /**
     * Gets current level
     *
     * @return floor
     */
    public Floor getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Sets current level
     *
     * @param floor floor
     */
    public void setCurrentFloor(final Floor floor) {
        this.currentFloor = floor;
    }

    /**
     * Gets Number of passengers
     *
     * @return count
     */
    public int getNumberOfPassengersInside() {
        return elevatorContainer.getPassengersCount();
    }

    /**
     * Notifies all passengers
     *
     * @param countDownLatch latch
     */
    public synchronized void doNotifyAll(final CountDownLatch countDownLatch) {
        for (TransportationTask task : elevatorContainer.getPassengersTasks()) {
            task.setCountDownLatch(countDownLatch);
            task.doNotify();
        }
    }

    /**
     * Passengers in container
     *
     * @return passengers
     */
    public Collection<Passenger> getPassengersInside() {
        return elevatorContainer.getPassengers();
    }

    /**
     * Returns true if container full
     *
     * @return status
     */
    public boolean isElevatorFull() {
        return (elevatorContainer.getPassengersCount() >= Building.ELEVATOR_CAPACITY);
    }
}

