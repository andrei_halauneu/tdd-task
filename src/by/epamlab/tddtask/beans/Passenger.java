package by.epamlab.tddtask.beans;

import by.epamlab.tddtask.controller.ElevatorController;
import by.epamlab.tddtask.enums.TransportationState;
import by.epamlab.tddtask.task.TransportationTask;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class Passenger {
    private int id;
    private int source;
    private int target;

    private TransportationTask task;
    private ElevatorController elevatorController;
    private TransportationState transportationState;

    private boolean notified;

    /**
     * Constructs passenger with params
     *
     * @param id     passenger id
     * @param source floor
     * @param target floor
     */
    public Passenger(final int id, final int source, final int target) {
        this.id = id;
        this.source = source;
        this.target = target;
        transportationState = TransportationState.NOT_STARTED;
        task = new TransportationTask(this);
    }

    /**
     * Returns id of passenger
     *
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns source floor of passenger
     *
     * @return source floor
     */
    public int getSource() {
        return source;
    }

    /**
     * Returns target floor of passenger
     *
     * @return target floor
     */
    public int getTarget() {
        return target;
    }

    /**
     * Returns transportation task
     *
     * @return transportation task
     */
    public TransportationTask getTransportationTask() {
        return task;
    }

    /**
     * Returns elevator controller
     *
     * @return elevator controller
     */
    public ElevatorController getElevatorController() {
        return elevatorController;
    }

    /**
     * Sets elevator controller that get in and get out the passenger from elevator
     *
     * @param elevatorController elevator controller
     */
    public void setElevatorController(final ElevatorController elevatorController) {
        this.elevatorController = elevatorController;
    }

    /**
     * Sets new notified state
     *
     * @param notified notified state
     */
    public void setNotified(final boolean notified) {
        this.notified = notified;
    }

    /**
     * Returns notified state
     *
     * @return notified state
     */
    public boolean isNotified() {
        return notified;
    }

    /**
     * Returns transportation state
     *
     * @return transportation state
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }

    /**
     * Sets new transportation state
     *
     * @param transportationState new transportation state
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Passenger passenger = (Passenger) o;

        if (id != passenger.id) {
            return false;
        }
        if (source != passenger.source) {
            return false;
        }
        if (target != passenger.target) {
            return false;
        }
        if (notified != passenger.notified) {
            return false;
        }
        if (task != null) {
            if (!task.equals(passenger.task)) {
                return false;
            }
        } else {
            if (passenger.task != null) {
                return false;
            }
        }
        if (elevatorController != null) {
            if (!elevatorController.equals(passenger.elevatorController)) {
                return false;
            }
        } else {
            if (passenger.elevatorController != null) {
                return false;
            }
        }
        return transportationState == passenger.transportationState;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + source;
        result = 31 * result + target;
        if (task != null) {
            result = 31 * result + task.hashCode();
        } else {
            result = 31 * result + 0;
        }
        if (elevatorController != null) {
            result = 31 * result + elevatorController.hashCode();
        } else {
            result = 31 * result + 0;
        }
        if (transportationState != null) {
            result = 31 * result + transportationState.hashCode();
        } else {
            result = 31 * result + 0;
        }
        if (notified) {
            result = 31 * result + 1;
        } else {
            result = 31 * result + 0;
        }
        return result;
    }
}
