package by.epamlab.tddtask.beans;

import by.epamlab.tddtask.task.TransportationTask;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class Floor {
    private int floorNumber;
    private Container dispatchStoryContainer;
    private Container arrivalStoryContainer;

    /**
     * Constructs floor
     *
     * @param floorNumber number
     */
    public Floor(final int floorNumber) {
        this.floorNumber = floorNumber;
        dispatchStoryContainer = new Container();
        arrivalStoryContainer = new Container();
    }

    /**
     * Return floor container.
     *
     * @return floor container
     */
    public Container getDispatchStoryContainer() {
        return dispatchStoryContainer;
    }

    /**
     * Returns floor number.
     *
     * @return floor number
     */
    public int getFloorNumber() {
        return floorNumber;
    }

    /**
     * Returns arrival container.
     *
     * @return arrival container
     */
    public Container getArrivalContainer() {
        return arrivalStoryContainer;
    }

    /**
     * Returns arrival container size.
     *
     * @return size
     */
    public int getArrivalContainerSize() {
        return getArrivalContainer().getPassengersCount();
    }

    /**
     * Notifies all passengers in dispatch container.
     *
     * @param countDownLatch latch
     */
    public void doNotifyAll(final CountDownLatch countDownLatch) {
        for (TransportationTask task : dispatchStoryContainer.getPassengersTasks()) {
            task.setCountDownLatch(countDownLatch);
            task.doNotify();
        }
    }

    /**
     * Adds passengers to dispatch container.
     *
     * @param passengers passengers
     */
    public void addPassengersToDispatcherContainer(final List<Passenger> passengers) {
        for (Passenger passenger : passengers) {
            dispatchStoryContainer.addPassenger(passenger);
        }
    }

    /**
     * Checks is the floor empty.
     *
     * @return status
     */
    public boolean isFloorEmpty() {
        return dispatchStoryContainer.getPassengersCount() == 0;
    }

    /**
     * Returns passengers from arrival container.
     *
     * @return passengers
     */
    public Collection<Passenger> getPassengersFromArrivalContainer() {
        return arrivalStoryContainer.getPassengers();
    }

    /**
     * Returns passengers from dispatch container.
     *
     * @return passengers
     */
    public Collection<Passenger> getPassengersFromDispatchContainer() {
        return dispatchStoryContainer.getPassengers();
    }

    /**
     * Adds passengers to arrival container.
     *
     * @param passengers passengers
     */
    public void addPassengersToArrivalContainer(final List<Passenger> passengers) {
        for (Passenger passenger : passengers) {
            arrivalStoryContainer.addPassenger(passenger);
        }
    }

    /**
     * Gets size of dispatch container
     *
     * @return count
     */
    public int getDispatchContainerSize() {
        return dispatchStoryContainer.getPassengersCount();
    }
}

