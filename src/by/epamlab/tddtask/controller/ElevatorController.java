package by.epamlab.tddtask.controller;


import by.epamlab.tddtask.beans.Building;
import by.epamlab.tddtask.beans.Elevator;
import by.epamlab.tddtask.beans.Floor;
import by.epamlab.tddtask.beans.Passenger;
import by.epamlab.tddtask.enums.ElevatorState;
import by.epamlab.tddtask.utils.CompleteTransportationValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andrei Halauneu on 03.08.2016.
 */
public class ElevatorController implements Runnable {
    private final Logger logger = LogManager.getLogger(ElevatorController.class);

    private final Elevator elevator;

    private boolean isLimit = false;
    private List<Passenger> passengersToGetOut = new ArrayList<>();
    private CompleteTransportationValidator validator = CompleteTransportationValidator.getInstance();

    /**
     * Constructs elevator controller
     *
     * @param elevator elevator
     */
    public ElevatorController(final Elevator elevator) {
        this.elevator = elevator;
    }

    /**
     * Gets elevator of controller
     *
     * @return elevator
     */
    public Elevator getElevator() {
        return elevator;
    }

    /**
     * Moves elevator up to the floor
     *
     * @param floor floor to up
     */
    public void moveElevatorUp(final Floor floor) {
        int nextFloor = floor.getFloorNumber();
        logger.info(ElevatorState.MOVING_ELEVATOR + " from " + elevator.getCurrentFloorNumber()
                + " to " + nextFloor + ". Elevator moves up.");

        elevator.setElevatorState(ElevatorState.MOVING_ELEVATOR);
        elevator.setCurrentFloor(floor);
    }

    /**
     * Moves elevator up by one floor
     */
    public void moveElevatorUp() {
        Floor nextFloor = Building.nextFloor();
        logger.info(ElevatorState.MOVING_ELEVATOR + " from " + elevator.getCurrentFloorNumber()
                + " to " + nextFloor.getFloorNumber() + ". Elevator moves up.");

        elevator.setElevatorState(ElevatorState.MOVING_ELEVATOR);
        elevator.setCurrentFloor(nextFloor);
    }

    /**
     * Moves elevator down to the floor
     *
     * @param floor floor to down
     */
    public void moveElevatorDown(final Floor floor) {
        int nextFloor = floor.getFloorNumber();
        logger.info(ElevatorState.MOVING_ELEVATOR + " from " + elevator.getCurrentFloorNumber()
                + " to " + nextFloor + ". Elevator moves down.");
        elevator.setElevatorState(ElevatorState.MOVING_ELEVATOR);
        elevator.setCurrentFloor(floor);
    }

    /**
     * Moves elevator down by one floor
     */
    public void moveElevatorDown() {
        Floor nextFloor = Building.previousFloor();
        logger.info(ElevatorState.MOVING_ELEVATOR + " from " + elevator.getCurrentFloorNumber()
                + " to " + nextFloor.getFloorNumber() + ". Elevator moves down.");
        elevator.setElevatorState(ElevatorState.MOVING_ELEVATOR);
        elevator.setCurrentFloor(nextFloor);
    }

    /**
     * Stops and de boards passengers
     */
    public void stopElevator() {
        elevator.setElevatorState(ElevatorState.DEBOARDING_OF_PASSENGER);
        logger.info(ElevatorState.DEBOARDING_OF_PASSENGER
                + " Elevator is ready to get out passengers");
    }

    /**
     * Notifies passengers inside about getting out
     *
     * @param countDownLatch count down latch to wait passengers
     */
    public void notifyPassengersInside(final CountDownLatch countDownLatch) {
        logger.info(elevator.getNumberOfPassengersInside()
                + " passengers were notified about getting out.");
        try {
            elevator.doNotifyAll(countDownLatch);
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Passenger asks to get out
     *
     * @param passenger passenger
     * @return true if passenger can get out
     */
    public boolean askToGetOut(final Passenger passenger) {
        if (passenger.getTarget() == elevator.getCurrentFloorNumber()) {
            passenger.setElevatorController(this);
            passengersToGetOut.add(passenger);
            logger.info("Passenger number " + passenger.getId() + " ask controller to let him out from elevator.");
            return true;
        }
        return false;
    }

    /**
     * Returns passengers that can get out
     *
     * @return list of passengers
     */
    public List<Passenger> getPassengersToGetOut() {
        return passengersToGetOut;
    }

    /**
     * Gets out passengers
     *
     * @param passenger passengers to get out
     * @param floor     floor to get out
     */
    public void getOut(final Passenger passenger, final Floor floor) {
        floor.getArrivalContainer().addPassenger(passenger);
        elevator.getElevatorContainer().removePassenger(passenger);
        passengersToGetOut.remove(passenger);
        logger.info(ElevatorState.DEBOARDING_OF_PASSENGER + " passenger number " + passenger.getId()
                + " on " + floor.getFloorNumber() + ". Controller gets out passenger");
    }

    /**
     * Notifies passengers on the floor
     *
     * @param floor          floor with passengers to notify
     * @param countDownLatch count down latch to wait passengers
     */
    public void notifyPassengersOutside(final Floor floor, final CountDownLatch countDownLatch) {
        elevator.setElevatorState(ElevatorState.BOARDING_OF_PASSENGER);
        logger.info(elevator.getElevatorState() + " Elevator is ready to take passengers");
        logger.info(floor.getDispatchStoryContainer().getPassengersCount()
                + " passengers were notified about getting in on the floor number " + floor.getFloorNumber());
        floor.doNotifyAll(countDownLatch);

        }
    /**
     * Passenger asks to get in
     *
     * @return true if passenger can get in
     */
    public boolean askToGetIn() {
        if (elevator.isElevatorFull() || elevator.getCurrentFloor().isFloorEmpty()) {
            logger.info("Controller can not get in passenger. Elevator is full or floor is empty");
            elevator.setElevatorState(ElevatorState.MOVING_ELEVATOR);
            return false;
        }
        return true;
    }

    /**
     * Gets in passenger from the floor
     *
     * @param passenger passenger to get in
     * @param floor     floor to get passenger from
     */
    public void getIn(final Passenger passenger, final Floor floor) {
        floor.getDispatchStoryContainer().removePassenger(passenger);
        elevator.getElevatorContainer().addPassenger(passenger);
        logger.info(ElevatorState.BOARDING_OF_PASSENGER + " passenger number "
                + passenger.getId() + " on " + floor.getFloorNumber()
                + " floor. Controller takes passenger");
        logger.info("In the elevator now " + elevator.getNumberOfPassengersInside() + " passengers");
    }

    @Override
    public void run() {
        logger.info(ElevatorState.STARTING_TRANSPORTATION + ". Transportation started.");
        while (!Thread.currentThread().isInterrupted()) {
            try {
                execute();
            } catch (RuntimeException e) {
                isLimit = !isLimit;
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private void execute() throws InterruptedException {
        CountDownLatch latch;
        if (!isLimit) {
            moveElevatorUp();
        } else {
            moveElevatorDown();
        }
        stopElevator();
        latch = new CountDownLatch(elevator.getNumberOfPassengersInside());
        notifyPassengersInside(latch);

        latch.await();

        latch = new CountDownLatch(elevator.getCurrentFloor().getDispatchContainerSize());
        notifyPassengersOutside(elevator.getCurrentFloor(), latch);

        latch.await();

        if (isTimeToStop()) {
           stopRunning();
        }
    }

    private void stopRunning() {
        logger.info(ElevatorState.COMPLETION_TRANSPORTATION);
        Thread.currentThread().interrupt();
    }

    private boolean isTimeToStop() {
        if (validator.isElevatorContainerEmpty()
                && validator.isPassengersNumberEqual()
                && validator.isDispatcherContainersEmpty()
                && validator.isPassengersOnRightFloors()
                && validator.isPassengersStateCompleted()) {
            logger.info("Transport validation complete");
            return true;
        }
        return false;
    }
}
